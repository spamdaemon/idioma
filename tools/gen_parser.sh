#!/bin/bash

#set -x

message()
{ echo "$@" 2>&1; }


usage()
{
    message "$0 [-k] [-B <bison-exe>] [-F <flex-exe>] [-y <bisonfile>] [-l <flexfile>] [-i<include dir>]";
    exit 1;
}

BISON_EXE="$(which bison 2>/dev/null)"
FLEX_EXE="$(which flex 2>/dev/null)"

TEMP_DIR="$(mktemp -dt "idioma.XXXXXX")";

FLEX_PREFIX=grammar
NAMESPACE=grammar
declare -a OPEN_NAMESPACE;
declare -a CLOSE_NAMESPACE;
GRAMMAR_CLASS=
PARSER_CLASS=

HEADER="${TEMP_DIR}/grammar.h";
BISON_INPUT="${TEMP_DIR}/grammar.y";
FLEX_INPUT="${TEMP_DIR}/grammar.l";
BISON_HEADER="${TEMP_DIR}/bison.h";
BISON_IMPL="${TEMP_DIR}/bison.cpp";
FLEX_IMPL="${TEMP_DIR}/flex.cpp";

set_grammar()
{
    GRAMMAR_CLASS="$1";
    PARSED_AST="idioma::parser::ParseTree";
    PARSER_CLASS="idioma::Parser";
}

set_namespace()
{
    case "${1}" in
	::*) NAMESPACE="${1/::/}";;
	*) NAMESPACE="${1}";;
    esac;
    FLEX_PREFIX="${NAMESPACE//::/_}";
    for f in ${1//::/ }; do
	OPEN_NAMESPACE+=("$(echo -e "namespace ${f} {\n")");
	CLOSE_NAMESPACE+=("$(echo -e "}\n")");
    done;
}

gen_bison()
{
    cat > "${BISON_INPUT}" <<EOF
%require "${BISON_VERSION}"
%language "c++"
%locations

%code requires {
#include "grammar.h"
#include <${GRAMMAR_INCLUDE}>
#include <idioma/parser/ParseTree.h>
#include <idioma/Parser.h>
#include <idioma/Scanner.h>
}

%code {

#define mkGrp ::idioma::parser::ParseTree::createGroup
#define mkIgn ::idioma::parser::ParseTree::createIgnorable
#define mkTok ::idioma::parser::ParseTree::createToken
#define join  ::idioma::parser::ParseTree::join


static int yylex(std::shared_ptr< ${PARSED_AST}>* token, ${NAMESPACE}::parser::location_type* location, idioma::Scanner& scanner)
{
  idioma::Scanner::Token tok = scanner.nextToken();
  *location  = ${NAMESPACE}::parser::location_type(
  	     ${NAMESPACE}::position(nullptr,1+tok.start.line,1+tok.start.column),
  	     ${NAMESPACE}::position(nullptr,1+tok.end.line,1+tok.end.column));
  
  auto start = ::std::make_shared< idioma::ast::SourceLocation>(tok.start.source,1+tok.start.line,1+tok.start.column);
  auto end  = ::std::make_shared< idioma::ast::SourceLocation>(tok.end.source,1+tok.end.line,1+tok.end.column);
  *token = mkTok(tok.text,start,end);
  return tok.code;
}

${OPEN_NAMESPACE[@]}

std::unique_ptr<idioma::Scanner> createScanner(FILE* file, const ::std::string* text, const ::std::string& src);
inline std::unique_ptr<idioma::Scanner> createFileScanner(FILE* file, const ::std::string& src)
{ return createScanner(file,0, src); }
inline std::unique_ptr<idioma::Scanner> createTextScanner(const ::std::string& text, const ::std::string& src)
{ return createScanner(0, &text, src); }

std::unique_ptr< ${PARSER_CLASS} > createParser()
{
  struct ParserImpl : public ${PARSER_CLASS} {
    ParserImpl() :
      _errors(0),
      _root()
      {}
    ~ParserImpl()  {}

    bool  hasErrors() const
    { return _errors!=0; }
   
    
    std::shared_ptr< ${PARSED_AST} > parseFile(const ::std::string& file)
    {
      FILE* fp = fopen(file.c_str(),"r");
      if (fp==NULL) {
      	 return nullptr;
      }
      std::unique_ptr<idioma::Scanner> scanner = createFileScanner(fp,file);
      parser xparser(*scanner,_root);
      if(0!=xparser.parse()) {
	++_errors;
      }
      if (scanner->hasErrors()) {
      	 ++_errors;
      }
      fclose(fp);
      
      if (hasErrors()) {
	::std::cerr << "Syntax errors found" << ::std::endl;
	_root = nullptr;
      }
      return _root;
    }

    std::shared_ptr< ${PARSED_AST} > parseText(const ::std::string& text, const ::std::string& srcInfo)
    {
      std::unique_ptr<idioma::Scanner> scanner = createTextScanner(text,srcInfo);
      parser xparser(*scanner,_root);
      if(0!=xparser.parse()) {
	++_errors;
      }
      if (scanner->hasErrors()) {
      	 ++_errors;
      }
      
      if (hasErrors()) {
	::std::cerr << "Syntax errors found" << ::std::endl;
	_root = nullptr;
      }
      return _root;
    }
    size_t _errors;
    std::shared_ptr< ${PARSED_AST} > _root;
  };

  std::unique_ptr< ${PARSER_CLASS} > res(new ParserImpl());
  return res;
}
${CLOSE_NAMESPACE[@]}


}

%parse-param { idioma::Scanner& lexer }
%parse-param { std::shared_ptr< ${PARSED_AST} >& ast }
%lex-param   { idioma::Scanner& lexer }

%define api.value.type { std::shared_ptr< ${PARSED_AST} > }
%define parse.error verbose

EOF
    
    cat < "${1}" >> "${BISON_INPUT}";
    cat >> "${BISON_INPUT}" <<EOF
%%
${OPEN_NAMESPACE[@]}
  void parser::error(const parser::location_type& location, const std::string& msg)
  { ::std::cerr << location << ":" << msg << ::std::endl; }
${CLOSE_NAMESPACE[@]}
EOF
}

gen_flex()
{
    cat > "${FLEX_INPUT}" <<EOF
%option noyywrap yylineno reentrant never-interactive
%option extra-type="idioma::Scanner::Position *"

%{
#include <idioma/Scanner.h>
#include "${BISON_HEADER}"

using tokens = ${NAMESPACE}::parser::token;

#define YY_USER_ACTION do { yyextra[0]=yyextra[1];yyextra[1].column += yyleng; } while(0);
%}

EOF
    cat < "${1}" >> "${FLEX_INPUT}";
    cat <<EOF >> "${FLEX_INPUT}"
%%

${OPEN_NAMESPACE[@]}
std::unique_ptr<idioma::Scanner> createScanner(FILE* file, const ::std::string* text, const ::std::string& src)
{
  
  struct Impl : public idioma::Scanner {
    Impl(FILE* xfile, const ::std::string& xSourceFile) : 
    	       errorCount(0),sourceFile(::idioma::Source::create(xSourceFile)),bufstate(0) {
      ${FLEX_PREFIX}lex_init(&${FLEX_PREFIX});
      bufstate = ${FLEX_PREFIX}_create_buffer(xfile,YY_BUF_SIZE,${FLEX_PREFIX});
      ${FLEX_PREFIX}push_buffer_state(bufstate,${FLEX_PREFIX});
      ${FLEX_PREFIX}set_extra(&position[0],${FLEX_PREFIX});
      position[0].source = sourceFile;
      position[1].source = sourceFile;
    }
    Impl(const ::std::string* xstr, const ::std::string& xSourceFile) : 
    	       errorCount(0),sourceFile(::idioma::Source::create(xSourceFile)),bufstate(0) {
      ${FLEX_PREFIX}lex_init(&${FLEX_PREFIX});
      if (xstr) {
            bufstate = ${FLEX_PREFIX}_scan_bytes(xstr->data(),(int)xstr->size(),${FLEX_PREFIX});
      } else {
            bufstate = ${FLEX_PREFIX}_scan_bytes(0,0,${FLEX_PREFIX});
      }
      ${FLEX_PREFIX}push_buffer_state(bufstate,${FLEX_PREFIX});
      ${FLEX_PREFIX}set_extra(&position[0],${FLEX_PREFIX});
      position[0].source = sourceFile;
      position[1].source = sourceFile;
    }
    ~Impl() {
      ${FLEX_PREFIX}lex_destroy(${FLEX_PREFIX});
    }

    Token nextToken()
    {
      while(true) {
	int tok_code=${FLEX_PREFIX}lex(${FLEX_PREFIX});
	std::string tok_text=${FLEX_PREFIX}get_text(${FLEX_PREFIX});
	if (tok_code<0) {
	  sourceFile->print(::std::cerr);
	  std::cerr << ':' << position[0].line << ':' << position[0].column << ": unexpected character " << tok_text << ::std::endl;
	  ++errorCount;
	  continue;
	}
	return idioma::Scanner::Token(tok_text,tok_code,position[0],position[1]);
      }
    }
    bool  hasErrors() const { return errorCount!=0; }

 private: 
    ::idioma::Scanner::Position position[2];
  
 private:
    size_t errorCount;

 private:
 const ::idioma::Source::CPtr sourceFile;
    
  private:
    yyscan_t ${FLEX_PREFIX};

    YY_BUFFER_STATE bufstate;
  };
  if (file) {
    return ::std::unique_ptr<idioma::Scanner>(new Impl(file,src));
  } else {
    return ::std::unique_ptr<idioma::Scanner>(new Impl(text,src));
  }
}
${CLOSE_NAMESPACE[@]}
EOF
}

gen_header()
{
NS="${NAMESPACE//::/_}";
    cat >> "${HEADER}" <<EOF
#ifndef _${NS^^}_H
#define _${NS^^}_H

#include <memory>
#include <idioma/Parser.h>
#include <${GRAMMAR_INCLUDE}>

${OPEN_NAMESPACE[@]}
 std::unique_ptr< ${PARSER_CLASS} > createParser();
${CLOSE_NAMESPACE[@]}

#endif

EOF
}


compile()
{
    cd "${TEMP_DIR}";
    gcc "${GCC_OPTS[@]}" -o"${BISON_IMPL}.o" "${BISON_IMPL}";
    if [ $? -ne 0 ]; then
	return 1;
    fi;
    gcc "${GCC_OPTS[@]}" -o"${FLEX_IMPL}.o"  "${FLEX_IMPL}";
    if [ $? -ne 0 ]; then
	return 1;
    fi;
    ld -Ur "${FLEX_IMPL}.o" "${BISON_IMPL}.o" -o grammar.o;
    return 0;
}


cleanup()
{
    if [ -n "${TEMP_DIR}" ]; then
	true;
	rm -rf "${TEMP_DIR}";
    fi;
}

trap cleanup EXIT;

declare -a BISON_OPTS;
declare -a FLEX_OPTS;
declare -a GCC_OPTS;

BISON_SOURCE=
FLEX_SOURCE=

OBJECT_OUTPUT=grammar.o;
HEADER_OUTPUT=grammar.h;
KEEP_OUTPUTS=false;
while getopts "B:F:G:g:I:y:l:O:H:n:k" opt; do
    case "$opt" in
	k) KEEP_OUTPUTS=true;;
	B) BISON_EXE="${OPTARG}";;
	F) FLEX_EXE="${OPTARG}";; 
	g) set_grammar "${OPTARG}";;
	G) GRAMMAR_INCLUDE="${OPTARG}";;
	I) GCC_OPTS+=("-I$(readlink -f "${OPTARG}")");;
	y) BISON_SOURCE="${OPTARG}";;
	l) FLEX_SOURCE="${OPTARG}";;
	n) NAMESPACE="${OPTARG}";;
	O) OBJECT_OUTPUT="${OPTARG}";;
	H) HEADER_OUTPUT="${OPTARG}";;
	*) message "Unexpected parameter $opt";
	   usage;;
    esac;
done;
shift $((OPTIND-1));

if [ -z "${BISON_EXE}" ]; then
    message "Missing bison executable";
    exit 1;
fi;

if [ -z "${FLEX_EXE}" ]; then
    message "Missing flex executable";
    exit 1;
fi;

if [ ! -x "${BISON_EXE}" ]; then
    message "No such executable ${BISON_EXE}";
    exit 1;
fi;

if [ ! -x "${FLEX_EXE}" ]; then
    message "No such executable ${FLEX_EXE}";
    exit 1;
fi;

if [ -z "${GRAMMAR_CLASS}" ]; then
    message "Missing grammar type";
    exit 1;
fi;
if [ -z "${GRAMMAR_INCLUDE}" ]; then
    message "Missing grammar include";
    exit 1;
fi;
if [ -z "${FLEX_SOURCE}" ]; then
    message "Flex source not provided";
    exit 1;
fi;
if [ -z "${BISON_SOURCE}" ]; then
    message "Bison source not provided";
    exit 1;
fi;
if [ ! -f "${FLEX_SOURCE}" ]; then
    message "Flex source is not readable";
    exit 1;
fi;
if [ ! -f "${BISON_SOURCE}" ]; then
    message "Bison source is not readable";
    exit 1;
fi;

BISON_VERSION="$("${BISON_EXE}" -V | head -n1 | grep -oE '[[:digit:]]+\..*$')";

set_namespace "${NAMESPACE}";
gen_bison "${BISON_SOURCE}"
gen_flex "${FLEX_SOURCE}"
gen_header;

if [[ "$BISON_VERSION" < "3.3" ]]; then
    BISON_OPTS+=("-Dparser_class_name={parser}")
else
    BISON_OPTS+=("-Dapi.parser.class={parser}")
fi;

BISON_OPTS+=("-Wcounterexamples")
BISON_OPTS+=("-Dapi.namespace={${NAMESPACE}}")
BISON_OPTS+=("--defines=${BISON_HEADER}")
BISON_OPTS+=("-o${BISON_IMPL}")

BISON_OPTS+=("-g${TEMP_DIR}/graph");
BISON_OPTS+=("-rall");
BISON_OPTS+=("--report-file=${TEMP_DIR}/report");

FLEX_OPTS+=("-o${FLEX_IMPL}") 
FLEX_OPTS+=("-P${FLEX_PREFIX}")
FLEX_OPTS+=("--nounistd");


"${BISON_EXE}" "${BISON_OPTS[@]}" "${BISON_INPUT}" &> "${TEMP_DIR}/gen.bison.out";
if [ $? -ne 0 ] || grep 'warning:' "${TEMP_DIR}/gen.bison.out" &> /dev/null; then
    cat "${TEMP_DIR}/gen.bison.out";
    message "bison generated warnings which are treated as errors";
    message "Leaving temporary directory $TEMP_DIR behind";
    TEMP_DIR=
    exit 1;
fi;
"${FLEX_EXE}" "${FLEX_OPTS[@]}" "${FLEX_INPUT}" &> "${TEMP_DIR}/gen.flex.out";
if [ $? -ne 0 ] || grep 'warning:' "${TEMP_DIR}/gen.flex.out" &> /dev/null; then
    cat "${TEMP_DIR}/gen.flex.out";
    message "flex generated warnings which are treated as errors";
    message "Leaving temporary directory $TEMP_DIR behind";
    TEMP_DIR=
    exit 1;
fi;

GCC_OPTS+=("-I${TEMP_DIR}")
GCC_OPTS+=("-I$(pwd)")
GCC_OPTS+=("-c");
GCC_OPTS+=("-fPIC");

# compile in a sub-shell
(compile)
if [ $? -ne 0 ]; then
    exit 1;
fi;

cp "${TEMP_DIR}/grammar.h" "${HEADER_OUTPUT}";
cp "${TEMP_DIR}/grammar.o" "${OBJECT_OUTPUT}";

if ${KEEP_OUTPUTS}; then
    message "Keeping temporary directory $TEMP_DIR";
    TEMP_DIR=
fi;
