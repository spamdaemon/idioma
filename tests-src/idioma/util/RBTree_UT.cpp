#include <idioma/util/RBTree.h>
#include <cassert>
#include <utility>

using RBTree = ::idioma::util::RBTree<int,int>;

// values in the range 0 to 15, inclusive
static const int values[] = { 13, 5, 1, 10, 3, 7, 8, 9, 2, 14, 6, 0, 15, 11, 4, 12, -1 };

static RBTree make_tree(const int *xvalues)
{
  RBTree t0;

  for (int i = 0; xvalues[i] >= 0; ++i) {
    t0 = t0.insert(xvalues[i], xvalues[i]);
    t0.check_tree();
  }
  assert(t0.check_tree() < 4);
  assert(t0.size() == 16);

  return t0;
}

void utest_create_empty()
{
  RBTree tree;

  assert(tree.contains(0) == false && "Expected no value");
  assert(tree.contains(0) == false && "Expected no value");
  assert(tree.remove(0) == tree && "Expected the same tree");
}

void utest_insert_1()
{
  RBTree t0;
  auto t1 = t0.insert(1, 2);
  assert(t0 != t1);
  assert(!t0.contains(1));
  assert(t1.contains(1));
  auto t2 = t1.remove(1);
  assert(t1.contains(1));
  assert(!t2.contains(1));
}

void utest_insert_many()
{
  RBTree t0 = make_tree(values);

  RBTree t1 = t0.insert(values[5], 2 * values[5]);
  assert(t1 == t0 && "Should not update an existing value");

  assert(t0.check_tree() < 4);
  assert(t0.size() == 16);
  for (int i = 0; values[i] >= 0; ++i) {
    typename RBTree::DataPtr data = t0.find(values[i]);
    assert(data && data->second == values[i]);
  }
}

void utest_remove()
{
  RBTree t0 = make_tree(values);
  for (int i = 0; values[i] >= 0; ++i) {
    t0 = t0.remove(values[i]);
    t0.check_tree();
  }
  assert(t0.size() == 0);
  assert(t0.check_tree() == 0);
}

void utest_insert_assign()
{
  RBTree t0 = make_tree(values);
  for (int i = 0; values[i] >= 0; ++i) {
    RBTree t1 = t0.insert_or_assign(values[i], 2 * values[i]);
    assert(t1 != t0 && "Should update an existing value");
  }
}

void utest_lower_bound()
{
  RBTree t0 = make_tree(values);

  {
    auto i = t0.lowerBound(9);
    assert(i);
    assert(i->first == 9);
  }

  {
    auto i = t0.lowerBound(-1);
    assert(i);
    assert(i->first == 0);
  }

  {
    auto i = t0.lowerBound(20);
    assert(i == nullptr);
  }

}

void utest_upper_bound()
{
  RBTree t0 = make_tree(values);

  {
    auto i = t0.upperBound(9);
    assert(i);
    assert(i->first == 10);
  }

  {
    auto i = t0.upperBound(-1);
    assert(i);
    assert(i->first == 0);
  }

  {
    auto i = t0.upperBound(15);
    assert(i == nullptr);
  }

}

void utest_min_max()
{
  RBTree t0 = make_tree(values);
  auto max = t0.max();
  assert(max && max->first == 15);
  auto min = t0.min();
  assert(min && min->first == 0);
}

void utest_count()
{
  RBTree t0 = make_tree(values);
  auto cnt1 = t0.count(10);
  assert(cnt1 == 1);
  auto cnt0 = t0.count(20);
  assert(cnt0 == 0);
}

void utest_elements()
{
  RBTree t0 = make_tree(values);
  auto elems = t0.elements();
  assert(elems.size() == t0.size());
  for (size_t i = 1; i < elems.size(); ++i) {
    assert(elems[i]->first == elems[i - 1]->first + 1);
  }
}
