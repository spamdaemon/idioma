#include <idioma/ast.h>
#include <idioma/visitor/NodePrinter.h>
#include <sstream>

using namespace idioma::ast;
using namespace idioma::visitor;

void utest_1()
{
  ::std::ostringstream out;
  Node::Ptr token = TokenNode::create("TOKEN", nullptr);
  Node::Ptr root = GroupNode::create("BAR", { { "FOO", token } });

  NodePrinter::print(root, out);
}
