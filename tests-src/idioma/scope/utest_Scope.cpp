#include <idioma/scope/Tree.h>
#include <idioma/scope/Scope.h>
#include <cassert>
#include <string>
#include <iostream>

using namespace idioma::scope;
using namespace idioma::util;

void utest_basic_basic_scope()
{
  Tree tree;
  auto root = tree.rootScope();
  assert(root && "No root scope");
  assert(!root->parent() && "Root should not have a parent");
  auto child = root->create();
  assert(child && "No child");
  assert(child->parent() == root && "Incorrect parent");
}

void utest_update_scope()
{
  Tree tree;
  auto root = tree.rootScope();
  root->put(::std::string("FOO"), ::std::string("BAR"));
  root->put(::std::string("Hello"), ::std::string("BAR"));
  auto scope = root->create(::std::string("Hello"), ::std::string("World"));
  assert(scope->search(::std::string("FOO")) == root && "Failed to search hierarchy");
  assert(scope->search(::std::string("Hello")) == scope && "Failed to search a hierarchy");
  assert(root->find("Hello").second && "Failed to perform a find");
  assert(root->find("Hello").first.cast<::std::string>()->value() == "BAR");
  assert(scope->lookup("Hello").second && "Failed to perform a lookup");
  assert(scope->lookup("Hello").first.cast<::std::string>()->value() == "World");
}

void utest_recursive_scope()
{
  Tree tree;
  auto ns0 = tree.rootScope();
  auto ns1 = ns0->create();
  auto ns2 = ns1->create();

  ns0->put(::std::string("ns1"), Value::create(ns1));
  ns1->put(::std::string("ns2"), Value::create(ns2));

  assert(ns2 == ns1->lookup("ns2").first.cast<Scope::Ptr>()->value());

}

void utest_scope_context()
{
  Tree tree;

  auto ns0 = tree.rootScope();
  auto ns1 = ns0->create( { Value(1.0), Value(true) });
  auto ns2 = ns1->create( { Value(2.0) });

  assert(ns2->findContext<double>().first->value() == 2.0);
  assert(ns2->findContext<bool>().first->value() == true);

}

