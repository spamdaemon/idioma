#include <idioma/ast/Visitor.h>
#include <idioma/ast.h>
#include <cassert>

using namespace idioma::ast;

struct Grammar
{
  typedef std::shared_ptr<Node> NodePtr;

  enum GroupType
  {
    SUM, MULT
  };
  enum RelationType
  {
    IGNORE, LHS, RHS
  };

  struct Rel
  {
    Node::Relation type;
    NodePtr node;

    Rel(RelationType t, NodePtr ptr)
        : type(t == IGNORE ? Node::getIgnorableKey() : Node::Relation(t)), node(ptr)
    {
    }
    Rel(NodePtr ptr)
        : Rel(IGNORE, ptr)
    {
    }
  };

public:
  static std::shared_ptr<const SourceLocation> location(SourceLocation::Line line,
      SourceLocation::Column col)
  {
    return ::std::make_shared<const SourceLocation>(line, col);
  }

public:
  static NodePtr token(const ::std::string &text, std::shared_ptr<const SourceLocation> start,
      std::shared_ptr<const SourceLocation> end)
  {
    return TokenNode::create(text, start, end);
  }

public:
  static NodePtr token(const ::std::string &text, std::shared_ptr<const SourceLocation> start)
  {
    return TokenNode::create(text, start);
  }

public:
  static NodePtr group(const GroupType &type, ::std::initializer_list<Rel> relations)
  {
    ::std::vector<GroupNode::Rel> rels;
    for (auto r : relations) {
      rels.emplace_back(r.type, r.node);
    }
    return GroupNode::create(type, rels);
  }
};

void utest_1()
{
  std::shared_ptr<const SourceLocation> start, end;
  Grammar::NodePtr lhs = Grammar::token("left", Grammar::location(1, 1), Grammar::location(1, 5));
  Grammar::NodePtr op = Grammar::token("+", Grammar::location(1, 5), Grammar::location(1, 6));
  Grammar::NodePtr rhs = Grammar::token("right", Grammar::location(1, 6), Grammar::location(1, 11));
  Grammar::NodePtr sum = Grammar::group(Grammar::SUM, { { Grammar::LHS, lhs }, { op }, {
      Grammar::RHS, rhs } })->copy();
  sum->print(std::cout);
  std::cout << ::std::endl;

  auto copy_of_sum = sum->copy();
  auto removeIgnorables1 = sum->removeIgnorables();
  assert(removeIgnorables1 != nullptr);
  auto removeIgnorables2 = removeIgnorables1->removeIgnorables();
  assert(removeIgnorables2 != nullptr);
  assert(removeIgnorables2 == removeIgnorables1);

  std::cout << ::std::endl;
  sum->getSpan(start, end);
  start->print(std::cout);
  ::std::cout << "->";
  end->print(std::cout);
  std::cout << ::std::endl;
}
