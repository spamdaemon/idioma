#ifndef _IDIOMA_VISITOR_NODEPRINTER_H
#define _IDIOMA_VISITOR_NODEPRINTER_H

#ifndef _IDIOMA_VISITOR_DEFAULTNODEVISITOR_H
#include <idioma/visitor/DefaultNodeVisitor.h>
#endif

#include <iosfwd>

namespace idioma {
  namespace visitor {

    class NodePrinter: public DefaultNodeVisitor
    {

    public:
      NodePrinter(::std::ostream &out);
      NodePrinter();
      ~NodePrinter();

    public:
      void visit(::std::shared_ptr<const idioma::ast::Node> node) override;
      void visit(::std::shared_ptr<const idioma::ast::TokenNode> node) override;

      /**
       * Print the specified node
       * @param  node the node to be printed
       * @param stream an output stream
       */
    public:
      static void print(::std::shared_ptr<const idioma::ast::Node> node, ::std::ostream &stream);

    public:
      bool printLocation;
      bool oneTokenPerLine;
      bool sortChildren;

      /** The output stream */
    private:
      ::std::ostream &_stream;

      /** The current line and column */
    private:
      size_t _lastLine, _lastCol;
    };
  }
}
#endif
