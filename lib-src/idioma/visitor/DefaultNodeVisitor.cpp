#include <idioma/visitor/DefaultNodeVisitor.h>

namespace idioma {
  namespace visitor {

    DefaultNodeVisitor::~DefaultNodeVisitor()
    {
    }

    void DefaultNodeVisitor::visit(idioma::ast::Visitor<const idioma::ast::Node>::Ptr)
    {
    }
    void DefaultNodeVisitor::visit(idioma::ast::Visitor<const idioma::ast::TokenNode>::Ptr ptr)
    {
      idioma::ast::Visitor<const idioma::ast::Node>::Ptr tmp(ptr);
      visit(tmp);
    }
    void DefaultNodeVisitor::visit(idioma::ast::Visitor<const idioma::ast::GroupNode>::Ptr ptr)
    {
      idioma::ast::Visitor<const idioma::ast::Node>::Ptr tmp(ptr);
      visit(tmp);
    }

  }
}
