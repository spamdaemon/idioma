#ifndef _IDIOMA_VISITOR_DEFAULTNODEVISITOR_H
#define _IDIOMA_VISITOR_DEFAULTNODEVISITOR_H

#ifndef _IDIOMA_AST_H
#include <idioma/ast.h>
#endif

namespace idioma {
  namespace visitor {

    /**
     * A node in the abstract syntax tree. Nodes only have pointer to their
     * children, but there is no way to get back to the parent.
     */
    class DefaultNodeVisitor: public ::idioma::ast::NodeVisitor, public ::idioma::ast::Visitor<
        const ::idioma::ast::Node>, public ::idioma::ast::Visitor<const ::idioma::ast::TokenNode>,
        public ::idioma::ast::Visitor<const ::idioma::ast::GroupNode>
    {
    public:
      virtual ~DefaultNodeVisitor() = 0;

    public:
      void visit(idioma::ast::Visitor<const idioma::ast::Node>::Ptr ptr) override;
      void visit(idioma::ast::Visitor<const idioma::ast::TokenNode>::Ptr ptr) override;
      void visit(idioma::ast::Visitor<const idioma::ast::GroupNode>::Ptr ptr) override;
    };
  }
}
#endif
