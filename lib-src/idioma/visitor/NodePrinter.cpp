#include <idioma/visitor/NodePrinter.h>
#include <idioma/ast/SourceLocation.h>

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

namespace idioma {
  namespace visitor {

    static ::std::vector<idioma::ast::Node::CPtr> sortChildNodes(idioma::ast::Node::CPtr p)
    {
      ::std::vector<idioma::ast::Node::CPtr> res = p->children();
      ::std::sort(res.begin(), res.end(),
          [](const idioma::ast::Node::CPtr &a, const idioma::ast::Node::CPtr &b) {
            ::std::shared_ptr<const idioma::ast::SourceLocation> astart, bstart, end;
            if (a->getSpan(astart, end) && b->getSpan(bstart, end)) {
              if (astart->line != bstart->line) {
                return astart->line < bstart->line;
              }
              return astart->column < bstart->column;
            }
            return false;
          });
      return res;
    }

    NodePrinter::NodePrinter()
        : NodePrinter(::std::cout)
    {
    }
    NodePrinter::NodePrinter(::std::ostream &out)
        : printLocation(false), oneTokenPerLine(false), sortChildren(false), _stream(out),
            _lastLine(0), _lastCol(0)
    {
    }
    NodePrinter::~NodePrinter()
    {
    }

    void NodePrinter::visit(::std::shared_ptr<const idioma::ast::TokenNode> node)
    {
      ::std::shared_ptr<const idioma::ast::SourceLocation> start, end;
      bool haveSpan = node->getSpan(start, end);

      if (!printLocation && !oneTokenPerLine) {
        if (haveSpan) {
          while (_lastLine < start->line) {
            ++_lastLine;
            _stream << ::std::endl;
            _lastCol = 0;
          }
          while (_lastCol < start->column) {
            _stream << ' ';
            ++_lastCol;
          }
        }
        for (char ch : node->text) {
          if (ch == '\n') {
            _lastCol = 0;
            ++_lastLine;
          } else {
            ++_lastCol;
          }
          _stream << ch;
        }
        return;
      }

      if (printLocation && haveSpan) {
        _stream << start->line << ':' << start->column << ':';
      }
      ::std::cout << node->text;
      if (oneTokenPerLine) {
        _stream << ::std::endl;
      } else {
        _stream << ' ';
      }
    }

    void NodePrinter::visit(::std::shared_ptr<const idioma::ast::Node> node)
    {
      if (sortChildren) {
        for (auto n : sortChildNodes(node)) {
          n->accept(*this);
        }
      } else {
        node->visitChildren(*this);
      }
    }

    void NodePrinter::print(::std::shared_ptr<const idioma::ast::Node> node, ::std::ostream &out)
    {
      NodePrinter printer(out);
      node->accept(printer);
    }

  }
}
