#include <idioma/astutils/utils.h>

namespace idioma {
  namespace astutils {
    using namespace ::idioma::ast;

    namespace {
      void search(::std::vector<::std::shared_ptr<const idioma::ast::Node>> &res,
          const ::std::shared_ptr<const idioma::ast::Node> &node,
          ::std::function<bool(const ::std::shared_ptr<const idioma::ast::Node>&)> criteria,
          bool stopOnMatch)
      {
        if (criteria(node)) {
          res.push_back(node);
          if (stopOnMatch) {
            return;
          }
        }
        for (auto c : node->children()) {
          search(res, c, criteria, stopOnMatch);
        }
      }
    }

    ::std::vector<::std::shared_ptr<const idioma::ast::Node>> findAll(
        const ::std::shared_ptr<const idioma::ast::Node> &node,
        ::std::function<bool(const ::std::shared_ptr<const idioma::ast::Node>&)> criteria)
    {
      ::std::vector<::std::shared_ptr<const idioma::ast::Node>> res;
      search(res, node, criteria, false);
      return res;
    }

    ::std::vector<::std::shared_ptr<const idioma::ast::Node>> findFirst(
        const ::std::shared_ptr<const idioma::ast::Node> &node,
        ::std::function<bool(const ::std::shared_ptr<const idioma::ast::Node>&)> criteria)
    {
      ::std::vector<::std::shared_ptr<const idioma::ast::Node>> res;
      search(res, node, criteria, true);
      return res;
    }

    ::std::shared_ptr<const TokenNode> getToken(const ::std::shared_ptr<const Node> &node,
        const Node::Relation &rel)
    {
      auto res = node->getNode(rel);
      return ::std::dynamic_pointer_cast<const TokenNode>(res);
    }

    ::std::vector<::std::shared_ptr<const TokenNode>> getTokens(
        const ::std::shared_ptr<const Node> &node, const Node::Relation &rel)
    {
      ::std::vector<::std::shared_ptr<const TokenNode>> res;
      for (auto n : node->getNodes(rel)) {
        auto t = ::std::dynamic_pointer_cast<const TokenNode>(n);
        if (t) {
          res.push_back(t);
        }
      }
      return res;
    }

    ::std::shared_ptr<const GroupNode> getGroup(const ::std::shared_ptr<const Node> &node,
        const Node::Relation &rel)
    {
      auto res = node->getNode(rel);
      return ::std::dynamic_pointer_cast<const GroupNode>(res);
    }

    ::std::vector<::std::shared_ptr<const GroupNode>> getGroups(
        const ::std::shared_ptr<const Node> &node, const Node::Relation &rel)
    {
      ::std::vector<::std::shared_ptr<const GroupNode>> res;
      for (auto n : node->getNodes(rel)) {
        auto g = ::std::dynamic_pointer_cast<const GroupNode>(n);
        if (g) {
          res.push_back(g);
        }
      }
      return res;
    }

  }
}
