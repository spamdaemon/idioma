#ifndef _IDIOMA_ASTUTILS_UTILS_H
#define _IDIOMA_ASTUTILS_UTILS_H

#include <idioma/ast.h>

namespace idioma {
  namespace astutils {

    /**
     * Find all nodes that satisfy the specified criteria. Even if a node satisfies the criteria, its children may also be returned if they match the criteria.
     * @param node a node
     * @param criteria the search criteria
     * @return a vector nodes
     */
    ::std::vector<::std::shared_ptr<const idioma::ast::Node>> findAll(
        const ::std::shared_ptr<const idioma::ast::Node> &node,
        ::std::function<bool(const ::std::shared_ptr<const idioma::ast::Node>&)> criteria);

    /**
     * Find all nodes that satisfy the search criteria, but if a node is found, its children will not be search.
     * @param node a node
     * @param criteria the search criteria
     * @return a vector nodes
     */
    ::std::vector<::std::shared_ptr<const idioma::ast::Node>> findFirst(
        const ::std::shared_ptr<const idioma::ast::Node> &node,
        ::std::function<bool(const ::std::shared_ptr<const idioma::ast::Node>&)> criteria);

    /**
     * Get the relation that must be a token.
     * @param node the parent node, which satisfy isUsableNode()
     * @param rel the relation type
     * @return the token node or null if not a token or the relation does not exist
     */
    ::std::shared_ptr<const idioma::ast::TokenNode> getToken(
        const ::std::shared_ptr<const idioma::ast::Node> &node,
        const idioma::ast::Node::Relation &rel);
    ::std::vector<::std::shared_ptr<const idioma::ast::TokenNode>> getTokens(
        const ::std::shared_ptr<const idioma::ast::Node> &node,
        const idioma::ast::Node::Relation &rel);

    /**
     * Get the relation that must be a bgroup.
     * @param node the parent node, which satisfy isUsableNode()
     * @param rel the relation type
     * @return the token node or null if not a group or the relation does not exist
     */
    ::std::shared_ptr<const idioma::ast::GroupNode> getGroup(
        const ::std::shared_ptr<const idioma::ast::Node> &node,
        const idioma::ast::Node::Relation &rel);

    /**
     * Get the relation that must be a bgroup.
     * @param node the parent node, which satisfy isUsableNode()
     * @param rel the relation type
     * @return the token node or null if not a group or the relation does not exist
     */
    ::std::vector<::std::shared_ptr<const idioma::ast::GroupNode>> getGroups(
        const ::std::shared_ptr<const idioma::ast::Node> &node,
        const idioma::ast::Node::Relation &rel);
  }
}
#endif
