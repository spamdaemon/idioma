#ifndef _IDIOMA_AST_H
#define _IDIOMA_AST_H

#ifndef _IDIOMA_AST_TOKENNODE_H
#include <idioma/ast/TokenNode.h>
#endif

#ifndef _IDIOMA_AST_GROUPNODE_H
#include <idioma/ast/GroupNode.h>
#endif

#ifndef _IDIOMA_AST_NODEVISITOR_H
#include <idioma/ast/NodeVisitor.h>
#endif

#endif
