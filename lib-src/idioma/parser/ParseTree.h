#ifndef _IDIOMA_PARSER_PARSETREE_H
#define _IDIOMA_PARSER_PARSETREE_H

#ifndef _IDIOMA_AST_NODE_H
#include <idioma/ast/Node.h>
#endif

#ifndef _IDIOMA_AST_GROUPNODE_H
#include <idioma/ast/GroupNode.h>
#endif

#ifndef _IDIOMA_AST_SOURCELOCATION_H
#include <idioma/ast/SourceLocation.h>
#endif

#include <memory>
#include <vector>

namespace idioma {
  namespace parser {

    /**
     * A node in the abstract syntax tree. Nodes only have pointer to their
     * children, but there is no way to get back to the parent.
     */
    class ParseTree
    {
      /** A pointer to a node list */
    public:
      typedef ::std::shared_ptr<ParseTree> Ptr;

      /**
       * A Rel identifies one or more nodes with a key.
       */
      struct Rel
      {
        /** Create an ignorable relation */
        Rel(const ParseTree::Ptr &node);

        /** Create relation with a key */
        Rel(const idioma::ast::Node::Relation &rel, const Ptr &node);

        idioma::ast::Node::Relation rel;
        Ptr nodes;
      };

      /** An empty node list */
    public:
      ParseTree();

      /** Create a node list */
    public:
      ParseTree(::std::vector<idioma::ast::Node::Ptr> n);

      /** Destroy this node list */
    public:
      virtual ~ParseTree();

      /**
       * Get the ast node.
       * @return a single node
       * @throws exception if the list does not contain a single node
       */
    public:
      idioma::ast::Node::CPtr toTree() const;

      /**
       * Join two node lists
       * @param left a list (possibly nullptr)
       * @param right a list (possibly nullptr)
       * @return a or nullptr if both list are the nullpointer
       */
    public:
      static Ptr join(const Ptr &left, const Ptr &right);

      /**
       * Join multiple lists.
       * @param list
       * @return a or nullptr if both list are the nullpointer
       */
    public:
      static Ptr join(::std::initializer_list<Ptr> list);

      /**
       * Create a new node list
       * @return a new empty node list
       */
    public:
      static Ptr create(const idioma::ast::Node::Ptr &node = nullptr);

      /**
       * Create a new node list
       * @return a new empty node list
       */
    public:
      static Ptr createIgnorable(const Ptr &node = nullptr);

      /**
       * Create a node list containing a single group
       * @param type the type of the group
       */
    public:
      static Ptr createGroup(const idioma::ast::GroupNode::ID &xtype,
          ::std::initializer_list<Rel> rels);

      /**
       * Create a node list containing a single group
       * @param type the type of the group
       */
    public:
      static Ptr createToken(const ::std::string &xtext,
          const ::std::shared_ptr<const idioma::ast::SourceLocation> xstart,
          const ::std::shared_ptr<const idioma::ast::SourceLocation> &xend);

      /**
       * Create a new node list
       * @return a new empty node list
       */
    public:
      static Ptr create(::std::vector<idioma::ast::Node::Ptr> nodes);

      /** The nodes */
    public:
      const ::std::vector<idioma::ast::Node::Ptr> nodes;
    };
  }
}
#endif
