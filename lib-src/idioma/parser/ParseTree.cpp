#include <idioma/parser/ParseTree.h>
#include <idioma/ast/TokenNode.h>

#include <memory>
#include <vector>

namespace idioma {
  namespace parser {

    using namespace idioma::ast;

    ParseTree::Rel::Rel(const ParseTree::Ptr &xnodes)
        : rel(Node::getIgnorableKey()), nodes(xnodes)
    {
      if (!nodes) {
        nodes = ::std::make_shared<ParseTree>();
      }
    }

    ParseTree::Rel::Rel(const Node::Relation &xrel, const ParseTree::Ptr &xnodes)
        : rel(xrel), nodes(xnodes)
    {
      if (!nodes) {
        nodes = ::std::make_shared<ParseTree>();
      }
    }

    ParseTree::ParseTree()
    {
    }

    ParseTree::ParseTree(::std::vector<Node::Ptr> n)
        : nodes(::std::move(n))
    {
    }

    ParseTree::~ParseTree()
    {
    }

    idioma::ast::Node::CPtr ParseTree::toTree() const
    {
      if (nodes.size() != 1) {
        return nullptr;
      }
      return nodes[0];
    }

    ParseTree::Ptr ParseTree::join(const Ptr &left, const Ptr &right)
    {
      if (left == nullptr) {
        return right;
      }
      if (right == nullptr) {
        return left;
      }
      if (left->nodes.empty()) {
        return right;
      }
      if (right->nodes.empty()) {
        return left;
      }
      ::std::vector<Node::Ptr> n;
      n.insert(n.end(), left->nodes.begin(), left->nodes.end());
      n.insert(n.end(), right->nodes.begin(), right->nodes.end());
      return create(::std::move(n));
    }

    ParseTree::Ptr ParseTree::join(::std::initializer_list<Ptr> list)
    {
      if (list.size() == 0) {
        return nullptr;
      }
      ::std::vector<Node::Ptr> n;
      for (auto l : list) {
        if (l) {
          n.insert(n.end(), l->nodes.begin(), l->nodes.end());
        }
      }
      return create(::std::move(n));
    }

    ParseTree::Ptr ParseTree::create(const idioma::ast::Node::Ptr &ptr)
    {
      if (ptr) {
        return ::std::make_shared<ParseTree>(::std::vector<idioma::ast::Node::Ptr>( { ptr }));
      } else {
        return ::std::make_shared<ParseTree>();
      }
    }

    ParseTree::Ptr ParseTree::createIgnorable(const Ptr &nodes)
    {
      auto key = Node::getIgnorableKey();
      Node::Ptr n;
      if (nodes && !nodes->nodes.empty()) {
        n = GroupNode::create(key, { GroupNode::Rel(key, nodes->nodes) });
      } else {
        n = GroupNode::create(key);
      }
      return create(n);
    }

    ParseTree::Ptr ParseTree::createGroup(const idioma::ast::GroupNode::ID &xtype,
        ::std::initializer_list<Rel> rels)
    {
      ::std::vector<GroupNode::Rel> children;
      for (const Rel &r : rels) {
        children.push_back(GroupNode::Rel(r.rel, r.nodes->nodes));
      }
      return create(GroupNode::create(xtype, std::move(children)));
    }

    ParseTree::Ptr ParseTree::createToken(const ::std::string &xtext,
        const ::std::shared_ptr<const idioma::ast::SourceLocation> xstart,
        const ::std::shared_ptr<const idioma::ast::SourceLocation> &xend)
    {
      return create(TokenNode::create(xtext, xstart, xend));
    }

    ParseTree::Ptr ParseTree::create(::std::vector<Node::Ptr> nodes)
    {
      return ::std::make_shared<ParseTree>(::std::move(nodes));
    }

  }
}
