#include <idioma/Source.h>
#include <string>

namespace idioma {

  Source::~Source()
  {
  }

  void Source::print(::std::ostream&) const
  {
  }

  Source::CPtr Source::create(const ::std::string &src)
  {
    struct Impl: public Source
    {
      Impl(const ::std::string &s)
          : _source(s)
      {
      }
      ~Impl()
      {
      }
      void print(::std::ostream &out) const
      {
        out << _source;
      }
    private:
      const ::std::string _source;
    };
    return ::std::make_shared<Impl>(src);
  }

}
