#ifndef _IDIOMA_UTIL_VALUE_H
#define _IDIOMA_UTIL_VALUE_H

#include <memory>
#include <string>

namespace idioma {
  namespace util {

    /** A key that is used to save objects in a util. */
    class Value
    {

      /** The key impl */
    private:
      struct ValueImpl
      {
        /** Compare two keys */
        virtual ~ValueImpl()=0;
      };

      /** The base for the typed value */
    public:
      template<class V> struct Object: public ValueImpl
      {
        /** The destructor */
      public:
        Object(const V &v)
            : _value(v)
        {
        }
        ~Object()
        {
        }

        /** Get the value */
      public:
        inline V& value()
        {
          return _value;
        }

        /** Get the value */
      public:
        inline const V& value() const
        {
          return _value;
        }

      private:
        V _value;
      };

      /** A null-value */
    public:
      inline Value()
      {
      }

      /** A value with the specified string. */
    public:
      Value(const ::std::string &str);

      /**
       * Create a string value
       * @parm str a string or nullptr to create a null value
       */
    public:
      Value(const char *str);

      /** A boolean valued */
    public:
      Value(bool id);

      /** A numeric value */
    public:
      Value(int id);

      /**
       * Create a value
       */
    public:
      template<class V>
      Value(const V &v)
          : Value(create(v))
      {
      }

      /**
       * Create a value
       * @param templ a template
       */
    public:
      template<class V>
      static Value create(const V &v)
      {
        Value res;
        struct Impl: public Object<V>
        {
          Impl(const V &the_value)
              : Object<V>(the_value)
          {
          }
          ~Impl()
          {
          }
        };
        res._value = ::std::move(::std::make_shared<Impl>(v));
        return res;
      }

      /** True if the value is set */
    public:
      inline bool isset() const
      {
        return _value != nullptr;
      }

      /** True if the value is set */
    public:
      inline operator bool() const
      {
        return isset();
      }

      /**
       * Get the value as the specified type
       * @return a pointer to an object or null if not of the specified type.
       */
    public:
      template<class V>
      inline ::std::shared_ptr<Object<V>> cast()
      {
        return std::dynamic_pointer_cast<Object<V>>(_value);
      }

      /**
       * Get the value as the specified type
       * @return a pointer to an object or null if not of the specified type.
       */
    public:
      template<class V>
      inline ::std::shared_ptr<const Object<V>> cast() const
      {
        return std::dynamic_pointer_cast<const Object<V>>(_value);
      }

      /** The value */
    private:
      ::std::shared_ptr<ValueImpl> _value;
    };
  }
}
#endif
