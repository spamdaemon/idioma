#include <idioma/util/Value.h>

namespace idioma {
  namespace util {

    Value::ValueImpl::~ValueImpl()
    {
    }

    Value::Value(const ::std::string &str)
        : Value(create(str))
    {

    }

    Value::Value(const char *str)
        : Value(str ? create(str) : Value())
    {
    }

    Value::Value(int id)
        : Value(create(id))
    {

    }

    Value::Value(bool b)
        : Value(create(b))
    {

    }

  }
}
