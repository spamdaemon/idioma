#ifndef _IDIOMA_UTIL_KEY_H
#define _IDIOMA_UTIL_KEY_H

#include <string>
#include <memory>

namespace idioma {
  namespace util {

    /** A key that is used to save objects in a util. */
    class Key
    {

      /** The key impl */
    private:
      struct KeyBase
      {
        /** Compare two keys */
        virtual ~KeyBase()=0;
        virtual bool equals(const KeyBase &other) const = 0;
      };

    public:
      template<class K> class KeyValue: public KeyBase
      {
      public:
        KeyValue(const K &the_key)
            : key(the_key)
        {
        }
      public:
        ~KeyValue()
        {
        }

      public:
        bool equals(const KeyBase &other) const
        {
          const KeyValue *impl = dynamic_cast<const KeyValue*>(&other);
          return impl && impl->key == key;
        }
      public:
        const K key;
      };

      /** No default constructor */
      Key() = delete;

      /**
       * Create a new key.
       * @param name the characters
       * @param unused
       */
    private:
      Key(const ::std::shared_ptr<const KeyBase> &key, bool)
          : _key(key)
      {
      }

      /**
       * Create a new key.
       * @param name the characters
       */
    public:
      Key(const ::std::shared_ptr<const KeyBase> &key)
          : Key(key, false)
      {
      }

      /**
       * A template object
       */
    public:
      template<class T>
      Key(const T &k)
          : Key(create(k))
      {
      }

      /** Check if two keys are the same */
    public:
      friend bool operator==(const Key &a, const Key &b)
      {
        return a._key->equals(*b._key);
      }

      /** Check if two keys are the same */
    public:
      friend bool operator!=(const Key &a, const Key &b)
      {
        return !a._key->equals(*b._key);
      }

      /**
       * Test if this key has the specified value.
       * @param v value
       * @return true if the value of this key matches the given value
       */
    public:
      bool hasValue(const Key &v) const
      {
        return v == *this;
      }

      /**
       * Get the key value.
       * @return the value for this key
       */
    public:
      template<class K>
      ::std::shared_ptr<const KeyValue<K>> value() const
      {
        return ::std::dynamic_pointer_cast<const KeyValue<K>>(_key);
      }

      /**
       * Create a generic key.
       * @param k an object to be used as a key
       */
    public:
      template<class K>
      static Key create(const K &k)
      {
        /*
         * need to use a temporary with the correct type so the correct constructor
         * gets invoked. Otherwise, we get into an infinite loop.
         */
        ::std::shared_ptr<const KeyBase> kb = ::std::make_shared<KeyValue<K>>(k);
        return Key(kb, false);
      }

      /**
       * Create a generic for a literal string.
       * @param k an object to be used as a key
       */
    public:
      static Key create(const char *k)
      {
        return create(::std::string(k));
      }

      /** The key */
    private:
      ::std::shared_ptr<const KeyBase> _key;
    };
  }
}
#endif
