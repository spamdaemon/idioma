#ifndef _IDIOMA_PARSER_H
#define _IDIOMA_PARSER_H

#include <idioma/parser/ParseTree.h>
#include <string>

namespace idioma {

  class Parser
  {

    /** The default constructor */
  protected:
    Parser();

    /** Destructor */
  public:
    virtual ~Parser()=0;

    /**
     * Parse the specified file.
     * @param input
     * @return an node in the abstract syntax tree.
     */
    virtual idioma::parser::ParseTree::Ptr parseFile(const ::std::string &file) = 0;

    /**
     * Parse the specified stream.
     * @param stream
     * @param sourceInfo details the origin of the stream
     * @return an node in the abstract syntax tree.
     */
    virtual idioma::parser::ParseTree::Ptr parseStream(::std::istream &stream,
        const ::std::string &sourceInfo);

    /**
     * Parse the specified text.
     * @param text a text string
     * @param sourceInfo provides information about the source
     * @return an node in the abstract syntax tree.
     */
    virtual idioma::parser::ParseTree::Ptr parseText(const ::std::string &text,
        const ::std::string &sourceInfo) = 0;

    /**
     * Determine if there were any errors during the parse
     * @return true if there was parsing errors
     */
  public:
    virtual bool hasErrors() const = 0;
  };
}

#endif
