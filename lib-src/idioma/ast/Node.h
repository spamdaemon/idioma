#ifndef _IDIOMA_AST_NODE_H
#define _IDIOMA_AST_NODE_H

#ifndef _IDIOMA_AST_SOURCELOCATION_H
#include <idioma/ast/SourceLocation.h>
#endif

#ifndef _IDIOMA_AST_VISITOR_H
#include <idioma/ast/Visitor.h>
#endif

#ifndef _IDIOMA_UTIL_KEY_H
#include <idioma/util/Key.h>
#endif

#ifndef _IDIOMA_UTIL_VALUE_H
#include <idioma/util/Value.h>
#endif

#include <memory>
#include <initializer_list>
#include <list>
#include <vector>
#include <utility>
#include <functional>

namespace idioma {
  namespace ast {
    class NodeVisitor;

    /**
     * A node in the abstract syntax tree. Nodes only have pointer to their
     * children, but there is no way to get back to the parent.
     */
    class Node: public ::std::enable_shared_from_this<Node>
    {
      /** A span */
    public:
      typedef ::std::pair<::std::shared_ptr<const SourceLocation>,
          ::std::shared_ptr<const SourceLocation>> Span;

      /** A relation */
    public:
      typedef ::idioma::util::Key Relation;

      /** A node visitors */
    public:
      typedef idioma::ast::Visitor<const Node> Visitor;

    private:
      typedef ::std::pair<::idioma::util::Key, ::idioma::util::Value> KVPair;

    public:
      typedef ::std::vector<KVPair> Annotations;

      /** Delete the default copy operators */
    public:
      Node(const Node&) = delete;
      Node& operator=(const Node&) = delete;

      /** A pointer to a node */
    public:
      using Ptr= ::std::shared_ptr<Node>;
      using CPtr= ::std::shared_ptr<const Node>;

      /** A node list entry */
    public:
      struct Entry
      {
        Entry(const Relation &id, const CPtr &n);
        ~Entry();

        Relation rel;
        CPtr node;
      };

      /** A vector of nodes */
    public:
      typedef ::std::vector<Entry> Nodes;

      /**
       * Create a new node without children.
       */
    protected:
      Node();

      /**
       * Create a new node with the specified children.
       * @param childNodes the children of this node
       */
    protected:
      Node(Nodes childNodes);

      /** Destructor */
    public:
      virtual ~Node();

      /**
       * Create a copy of this node.
       * @return a copy of the tree rooted at this node
       */
    public:
      virtual Ptr copy() const = 0;

      /**
       * Get a relation key for ignorable nodes
       * @return the key that identifies ignorable nodes
       */
    public:
      static ::idioma::util::Key getIgnorableKey();

      /**
       * Remove all children whose relation is defined by a an ignorable key. This is one of the
       * only functions that can be used to modify the tree.
       * @return a tree that contains no ignorable nodes
       */
    public:
      virtual CPtr removeIgnorables() const = 0;

      /**
       * Test if this is an ignorable node
       * @return true if this node is ignorable
       */
    public:
      virtual bool isIgnorable() const;

      /**
       * Create a copy of this node.
       * @return a copy of the tree rooted at this node
       */
    public:
      virtual ::std::ostream& print(::std::ostream &out) const;

      /**
       * Get the start and end location of this node.
       * @param start the start location to be set
       * @param end the end location to be set location of the first character after this node
       * @return true if both start and end location were set
       */
    public:
      virtual bool getSpan(::std::shared_ptr<const SourceLocation> &start,
          ::std::shared_ptr<const SourceLocation> &end) const;

      /**
       * Get the start and end location of this node.
       * @return the span of the node
       */
    public:
      Span span() const;

      /** @name Node Annotations.
       * Annotations can be used to attached information to nodes, even nodes
       * that cannot be modified normally.
       */

      /**@{*/

      /**
       * Annotate this node.
       * @param key a key
       * @param value a value
       */
    public:
      void annotate(const ::idioma::util::Key &key, const ::idioma::util::Value &value) const;

      /**
       * Annotate this node.
       * @param key a key
       * @param value a value
       */
    public:
      void annotate(const Annotations &annotations) const;

      /**
       * Determine if this node was annotated.
       * @param key a key
       * @return true if this node was annotated
       */
    public:
      bool hasAnnotation(const ::idioma::util::Key &key) const;

      /**
       * Get an annotation
       * @param key a key
       * @return the annotion for the specified key or an empty value.
       */
    public:
      ::idioma::util::Value annotation(const ::idioma::util::Key &key) const;

      /**
       * Get an annotations
       * @return the annotions
       */
    public:
      Annotations annotations() const;

      /**@}*/

      /**
       * Accept the specified visitor.
       * @param self a pointer to self (is same as this)
       * @param v a visitor
       */
    public:
      virtual void accept(NodeVisitor &v) const;

      /**
       * Visit the children of this node
       * @param v a visitor
       */
    public:
      void visitChildren(NodeVisitor &v) const;

      /**
       * Get the children of this node.
       * @return a vector of children of this node
       */
    public:
      ::std::vector<CPtr> children() const;

      /**
       * Get the children of this node that satisfy the specified predicate.
       * @return a vector of children of this node
       */
    public:
      ::std::vector<CPtr> children(::std::function<bool(const CPtr&)> predicate) const;

      /** Get this node as the specified type.
       * @return this node
       */
    public:
      template<class T> ::std::shared_ptr<T> self()
      {
        return std::dynamic_pointer_cast<T>(shared_from_this());
      }

      template<class T> ::std::shared_ptr<const T> self() const
      {
        return std::dynamic_pointer_cast<const T>(shared_from_this());
      }

      inline const Nodes& entries() const
      {
        return nodes;
      }

      // get the first node that has the specified releation
      CPtr getNode(const Node::Relation &rel) const;

      // get all children that have the specified relation
      ::std::vector<CPtr> getNodes(const Relation &rel) const;

    private:
      Nodes nodes;

      /** The annotations */
    private:
      mutable Annotations _annotations;
    };
  }
}
#endif
