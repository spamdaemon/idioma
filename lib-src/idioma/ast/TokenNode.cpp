#include <idioma/ast/TokenNode.h>
#include <idioma/ast/NodeVisitor.h>

#include <iostream>
#include <memory>
#include <string>

namespace idioma {
  namespace ast {

    TokenNode::TokenNode(const ::std::string &xtext,
        const ::std::shared_ptr<const SourceLocation> xstart,
        const ::std::shared_ptr<const SourceLocation> &xend)
        : text(xtext), start(xstart), end(xend)
    {
    }

    TokenNode::~TokenNode()
    {
    }

    Node::CPtr TokenNode::removeIgnorables() const
    {
      if (isIgnorable()) {
        return nullptr;
      }
      return self<Node>();
    }

    Node::Ptr TokenNode::copy() const
    {
      Ptr this_copy = create(text, start, end);
      this_copy->annotate(annotations());
      return this_copy;
    }

    std::ostream& TokenNode::print(std::ostream &out) const
    {
      out << text << ' ';
      return out;
    }

    std::shared_ptr<TokenNode> TokenNode::create(const ::std::string &xtext,
        const ::std::shared_ptr<const SourceLocation> xstart,
        const ::std::shared_ptr<const SourceLocation> &xend)
    {
      return std::make_shared<TokenNode>(xtext, xstart, xend);
    }

    std::shared_ptr<TokenNode> TokenNode::create(const ::std::string &xtext,
        const ::std::shared_ptr<const SourceLocation> xstart)
    {
      return std::make_shared<TokenNode>(xtext, xstart, nullptr);
    }

    std::shared_ptr<TokenNode> TokenNode::create(const ::std::string &xtext)
    {
      return std::make_shared<TokenNode>(xtext, nullptr, nullptr);
    }
    std::shared_ptr<TokenNode> TokenNode::create(const ::std::string &xtext, const Span &span)
    {
      return create(xtext, span.first, span.second);
    }

    std::shared_ptr<TokenNode> TokenNode::create(char xtext,
        const ::std::shared_ptr<const SourceLocation> xstart)
    {
      char tmp[2];
      tmp[0] = xtext;
      tmp[1] = '\0';
      return std::make_shared<TokenNode>(tmp, xstart, nullptr);
    }

    std::shared_ptr<TokenNode> TokenNode::create(const char *xtext,
        const ::std::shared_ptr<const SourceLocation> xstart,
        const ::std::shared_ptr<const SourceLocation> &xend)
    {
      return std::make_shared<TokenNode>(::std::string(xtext), xstart, xend);
    }

    std::shared_ptr<TokenNode> TokenNode::create(const char *xtext,
        const ::std::shared_ptr<const SourceLocation> xstart)
    {
      return std::make_shared<TokenNode>(::std::string(xtext), xstart, nullptr);
    }

    bool TokenNode::getSpan(::std::shared_ptr<const SourceLocation> &xstart,
        ::std::shared_ptr<const SourceLocation> &xend) const
    {
      xstart = this->start;
      xend = this->end;
      return xstart && xend;
    }

    void TokenNode::accept(NodeVisitor &v) const
    {
      Visitor *visitor = dynamic_cast<Visitor*>(&v);
      if (visitor) {
        visitor->visit(self<TokenNode>());
      } else {
        Node::accept(v);
      }
    }

    ::std::list<std::shared_ptr<const TokenNode>> TokenNode::get(const Node &parent)
    {
      ::std::list<std::shared_ptr<const TokenNode>> res;
      auto sel = [](const CPtr &n) {return std::dynamic_pointer_cast<const TokenNode>(n)!=nullptr;};
      for (auto rel : parent.children(sel)) {
        res.push_back(std::dynamic_pointer_cast<const TokenNode>(rel));
      }
      return res;
    }

  }
}
