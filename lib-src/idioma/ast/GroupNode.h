#ifndef _IDIOMA_AST_GROUPNODE_H
#define _IDIOMA_AST_GROUPNODE_H

#include <initializer_list>
#include <list>
#include <memory>

#ifndef _IDIOMA_AST_NODE_H
#include <idioma/ast/Node.h>
#endif

#ifndef _IDIOMA_AST_VISITOR_H
#include <idioma/ast/Visitor.h>
#endif

#ifndef _IDIOMA_UTIL_KEY_H
#include <idioma/util/Key.h>
#endif

namespace idioma {
  namespace ast {

    /**
     * A node in the abstract syntax tree. Nodes only have pointer to their
     * children, but there is no way to get back to the parent.
     */
    class GroupNode: public Node
    {
      /** A node visitors */
    public:
      typedef idioma::ast::Visitor<const GroupNode> Visitor;

    public:
      typedef typename Node::Ptr Ptr;

    public:
      typedef ::idioma::util::Key ID;

      typedef Node::Entry Entry;

      /** A vector of nodes */
    public:
      typedef ::std::vector<Entry> Nodes;

      /**
       * A Rel identifies one or more nodes with a key.
       */
      struct Rel
      {

        /** Create an ignorable relation */
        Rel(const Node::CPtr &node);

        /** Create relation with a key */
        Rel(const ::idioma::util::Key &rel, const Node::CPtr &node);

        /** Create relation with a key */
        Rel(Nodes n);

        /** Create relation with a key */
        template<class T>
        Rel(const ::idioma::util::Key &rel, const ::std::vector<T> &xnodes)
        {
          nodes.reserve(xnodes.size());
          for (auto n : xnodes) {
            if (n) {
              nodes.emplace_back(rel, n);
            }
          }
        }
        Nodes nodes;
      };

      /**
       * Create a new group.
       * @param type the type of the group
       */
    public:
      GroupNode(const ID &xtype);

      /**
       * Create a new group.
       * @param type the type of the group
       */
    public:
      GroupNode(const ID &xtype, const Rel &xchild);

      /**
       * Create a new group.
       * @param type the type of the group
       * @param type the type of the group
       */
    public:
      GroupNode(const ID &xtype, Nodes nodes);

      /** Destructor */
    public:
      ~GroupNode();

      /**
       * Check if this node has the specified id
       * @param xtype the type
       * @return true if id == xtype
       */
    public:
      inline bool hasType(const ID &xtype) const
      {
        return type == xtype;
      }

      /**
       * Create an ignorable node
       */
    public:
      static ::std::shared_ptr<GroupNode> createIgnorable();

      /**
       * Create a new group.
       * @param type the type of the group
       */
    public:
      static ::std::shared_ptr<GroupNode> create(const ID &xtype);

      /**
       * Create a new group.
       * @param type the type of the group
       */
    public:
      static ::std::shared_ptr<GroupNode> create(const ID &xtype, const ::std::vector<Rel> &rels);

      /**
       * Create a new group.
       * @param type the type of the group
       * @param children the child nodes
       */
    public:
      static ::std::shared_ptr<GroupNode> createGroup(const ID &xtype, Nodes children);

      /**
       * Get all children of the parent node that are groups and whose group id is the specified type.
       * @param parent the parent node whose children to inspect
       * @param group the group type.
       * @return a list of GroupNode
       */
    public:
      static ::std::list<std::shared_ptr<const GroupNode>> get(const Node &parent, const ID &group);

      /**
       * Create a new instance of this group, with the new children.
       * @param c the new children
       * @return a new instanceof this group
       */
    public:
      virtual ::std::shared_ptr<GroupNode> copyGroup(Nodes c) const;

      /**
       * Create a new instance of this group, with the new children.
       * @return a new instanceof this group
       */
    public:
      ::std::shared_ptr<GroupNode> copyGroup() const;

    public:
      void accept(NodeVisitor &v) const override;
      bool isIgnorable() const override;
      CPtr removeIgnorables() const override;
      Ptr copy() const override;

      /** The group type */
    public:
      const ID type;
    };
  }
}
#endif
