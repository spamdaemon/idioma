#ifndef _IDIOMA_AST_TOKENNODE_H
#define _IDIOMA_AST_TOKENNODE_H

#ifndef _IDIOMA_AST_NODE_H
#include <idioma/ast/Node.h>
#endif

#ifndef _IDIOMA_AST_VISITOR_H
#include <idioma/ast/Visitor.h>
#endif

#ifndef _IDIOMA_UTIL_KEY_H
#include <idioma/util/Key.h>
#endif

namespace idioma {
  namespace ast {

    /**
     * A node in the abstract syntax tree. Nodes only have pointer to their
     * children, but there is no way to get back to the parent.
     */
    class TokenNode: public Node
    {
      /** A node visitors */
    public:
      typedef idioma::ast::Visitor<const TokenNode> Visitor;

    public:
      typedef ::idioma::util::Key ID;

    public:
      typedef typename Node::Ptr Ptr;

      /**
       * Create a new token.
       * @param xtext the token text
       * @param xstart the start location
       * @param xend the end location
       */
    public:
      TokenNode(const ::std::string &xtext, const ::std::shared_ptr<const SourceLocation> xstart,
          const ::std::shared_ptr<const SourceLocation> &xend);

      /** Destructor */
    public:
      ~TokenNode();

      /** The copy operation for a token can be the returned token */
    public:
      Ptr copy() const override;
      CPtr removeIgnorables() const override;

    public:
      std::ostream& print(std::ostream &out) const;

      /**
       * Create a new token.
       * @param xtext the token text
       */
    public:
      static std::shared_ptr<TokenNode> create(const ::std::string &xtext);

      /**
       * Create a new token with the specified span.
       * @param xtext the token text
       */
    public:
      static std::shared_ptr<TokenNode> create(const ::std::string &xtext, const Span &span);

      /**
       * Create a new token.
       * @param xtext the token text
       * @param xstart the start location
       * @param xend the end location
       */
    public:
      static std::shared_ptr<TokenNode> create(const ::std::string &xtext,
          const ::std::shared_ptr<const SourceLocation> xstart,
          const ::std::shared_ptr<const SourceLocation> &xend);

      /**
       * Create a new token.
       * @param xtext the token text
       * @param xstart the start location
       * @param xend the end location
       */
    public:
      static std::shared_ptr<TokenNode> create(const ::std::string &xtext,
          const ::std::shared_ptr<const SourceLocation> xstart);

      /**
       * Create a new token for a single character
       * @param xtext the token character
       * @param xstart the character location
       */
    public:
      static std::shared_ptr<TokenNode> create(const char xtext,
          const ::std::shared_ptr<const SourceLocation> xstart);

      /**
       * Create a new token.
       * @param xtext the token text
       * @param xstart the start location
       * @param xend the end location
       */
    public:
      static std::shared_ptr<TokenNode> create(const char *xtext,
          const ::std::shared_ptr<const SourceLocation> xstart,
          const ::std::shared_ptr<const SourceLocation> &xend);

      /**
       * Create a new token.
       * @param xtext the token text
       * @param xstart the start location
       * @param xend the end location
       */
    public:
      static std::shared_ptr<TokenNode> create(const char *xtext,
          const ::std::shared_ptr<const SourceLocation> xstart);

      /**
       * Get the children of parent node that are tokens.
       * @param parent the parent node whose children to inspect
       * @return a list of TokenNode
       */
    public:
      static ::std::list<std::shared_ptr<const TokenNode>> get(const Node &parent);

      /**
       * Get the start and end location of this node.
       * @param start the start location to be set
       * @param end the end location to be set location of the first character after this node
       * @return true if both start and end location were set
       */
    public:
      bool getSpan(::std::shared_ptr<const SourceLocation> &xstart,
          ::std::shared_ptr<const SourceLocation> &xend) const;

    public:
      void accept(NodeVisitor &v) const;

      /** The token text */
    public:
      const ::std::string text;

      /** The start location */
    public:
      const ::std::shared_ptr<const SourceLocation> start;

      /** The end location */
    public:
      const ::std::shared_ptr<const SourceLocation> end;
    };
  }
}
#endif
