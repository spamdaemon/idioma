#ifndef _IDIOMA_AST_SOURCELOCATION_H
#define _IDIOMA_AST_SOURCELOCATION_H

#ifndef _IDIOMA_SOURCE_H
#include <idioma/Source.h>
#endif

#include <iostream>
#include <memory>

namespace idioma {
  namespace ast {

    /**
     * This class is used to represent the location of a single character.
     */
    class SourceLocation
    {
      /** The line counter */
    public:
      typedef size_t Line;
      /** The column type */
    public:
      typedef size_t Column;

      /**
       * Create a new source location.
       * @param src the source (may be null)
       * @param line the line (1 is the first line)
       * @param column the column (1 is the first column)
       */
    public:
      SourceLocation(::std::shared_ptr<const Source> source, Line line, Column column);

      /**
       * Create a new source location.
       * @param line the line (1 is the first line)
       * @param column the column (1 is the first column)
       */
    public:
      SourceLocation(Line line, Column column);

      /** Destructor */
    public:
      ~SourceLocation();

      /**
       * Print the source location.
       */
    public:
      std::ostream& print(std::ostream &out) const
      {
        if (source) {
          source->print(out);
          out << ':';
        }
        out << line << ':' << column;
        return out;
      }

      /**
       * Get the max of two locations.
       */
    public:
      static ::std::shared_ptr<const SourceLocation> maxOf(
          ::std::shared_ptr<const SourceLocation> a, ::std::shared_ptr<const SourceLocation> b)
      {
        if (!a && !b) {
          return a;
        }
        if (a && b) {
          if (a->line > b->line) {
            return a;
          }
          if (a->line < b->line) {
            return b;
          }
          if (a->column > b->column) {
            return a;
          }
          if (a->column < b->column) {
            return b;
          }
        }
        return a ? a : b;
      }

      /**
       * Get the max of two locations.
       */
    public:
      static ::std::shared_ptr<const SourceLocation> minOf(
          ::std::shared_ptr<const SourceLocation> a, ::std::shared_ptr<const SourceLocation> b)
      {
        if (!a && !b) {
          return a;
        }
        if (a && b) {
          if (a->line < b->line) {
            return a;
          }
          if (a->line > b->line) {
            return b;
          }
          if (a->column < b->column) {
            return a;
          }
          if (a->column > b->column) {
            return b;
          }
        }
        return a ? a : b;
      }

      /** The source */
    public:
      const ::std::shared_ptr<const Source> source;

      /** The line */
    public:
      const Line line;

      /** The column */
    public:
      const Column column;
    };
  }
}
#endif
