#ifndef _IDIOMA_AST_NODEVISITOR_H
#define _IDIOMA_AST_NODEVISITOR_H

namespace idioma {
  namespace ast {

    /**
     * A node in the abstract syntax tree. Nodes only have pointer to their
     * children, but there is no way to get back to the parent.
     */
    class NodeVisitor
    {
    public:
      virtual ~NodeVisitor() = 0;
    };
  }
}
#endif
