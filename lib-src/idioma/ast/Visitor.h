#ifndef _IDIOMA_AST_VISITOR_H
#define _IDIOMA_AST_VISITOR_H

#include <memory>

namespace idioma {
  namespace ast {

    /**
     * A node in the abstract syntax tree. Nodes only have pointer to their
     * children, but there is no way to get back to the parent.
     */
    template<class NODE>
    class Visitor
    {

      /** The pointer type */
    public:
      typedef ::std::shared_ptr<NODE> Ptr;

    public:
      virtual ~Visitor()
      {
      }

      /**
       * Visit a token
       * @param node a token node
       */
    public:
      virtual void visit(Ptr node) = 0;
    };
  }
}
#endif
