#include <idioma/ast/GroupNode.h>
#include <idioma/ast/NodeVisitor.h>

namespace idioma {
  namespace ast {

    GroupNode::Rel::Rel(const Node::CPtr &node)
    {
      if (node) {
        nodes.reserve(1);
        nodes.emplace_back(Node::getIgnorableKey(), node);
      }
    }

    GroupNode::Rel::Rel(const Node::Relation &rel, const Node::CPtr &node)
    {
      if (node) {
        nodes.reserve(1);
        nodes.emplace_back(rel, node);
      }
    }

    /** Create relation with a key */
    GroupNode::Rel::Rel(Nodes n)
        : nodes(::std::move(n))
    {
    }

    GroupNode::GroupNode(const ID &xtype)
        : Node(), type(xtype)
    {
    }

    GroupNode::GroupNode(const ID &xtype, const Rel &xchild)
        : Node(xchild.nodes), type(xtype)
    {
    }

    GroupNode::GroupNode(const ID &xtype, Nodes xnodes)
        : Node(::std::move(xnodes)), type(xtype)
    {
    }

    GroupNode::~GroupNode()
    {
    }

    Node::CPtr GroupNode::removeIgnorables() const
    {
      if (isIgnorable()) {
        return nullptr;
      }

      if (entries().empty()) {
        return self<Node>();
      }

      bool makeNew = false;
      Nodes res;
      res.reserve(entries().size());
      auto ignKey = getIgnorableKey();
      for (auto n : entries()) {
        if (n.rel == ignKey || n.node->isIgnorable()) {
          makeNew = true;
        } else {
          auto tree = n.node->removeIgnorables();
          if (!tree) {
            makeNew = true;
          } else {
            if (tree != n.node) {
              makeNew = true;
            }
            res.push_back(Entry(n.rel, tree));
          }
        }
      }
      if (makeNew) {
        return copyGroup(res);
      }
      return self<Node>();
    }

    ::std::shared_ptr<GroupNode> GroupNode::createIgnorable()
    {
      return ::std::make_shared<GroupNode>(Node::getIgnorableKey());
    }

    bool GroupNode::isIgnorable() const
    {
      return type == Node::getIgnorableKey();
    }

    Node::Ptr GroupNode::copy() const
    {
      auto this_copy = ::std::make_shared<GroupNode>(type, entries());
      this_copy->annotate(annotations());
      return this_copy;
    }

    ::std::shared_ptr<GroupNode> GroupNode::create(const ID &xtype)
    {
      return ::std::make_shared<GroupNode>(xtype);
    }

    ::std::shared_ptr<GroupNode> GroupNode::create(const ID &xtype, const ::std::vector<Rel> &rels)
    {
      Nodes xnodes;
      for (auto &r : rels) {
        xnodes.insert(xnodes.end(), r.nodes.begin(), r.nodes.end());
      }
      return createGroup(xtype, ::std::move(xnodes));
    }

    ::std::shared_ptr<GroupNode> GroupNode::createGroup(const ID &xtype, Nodes childNodes)
    {
      return ::std::make_shared<GroupNode>(xtype, ::std::move(childNodes));
    }

    ::std::list<std::shared_ptr<const GroupNode>> GroupNode::get(const Node &parent, const ID &t)
    {
      ::std::list<std::shared_ptr<const GroupNode>> res;
      auto sel = [t](const CPtr &n) {
        auto rel = std::dynamic_pointer_cast<const GroupNode>(n);
        return rel && rel->type == t;
      };

      for (auto rel : parent.children(sel)) {
        res.push_back(std::dynamic_pointer_cast<const GroupNode>(rel));
      }
      return res;
    }

    void GroupNode::accept(NodeVisitor &v) const
    {
      Visitor *visitor = dynamic_cast<Visitor*>(&v);
      if (visitor) {
        visitor->visit(self<GroupNode>());
      } else {
        Node::accept(v);
      }
    }

    ::std::shared_ptr<GroupNode> GroupNode::copyGroup(Nodes e) const
    {
      auto g = createGroup(type, ::std::move(e));
      g->annotate(annotations());
      return g;
    }

    ::std::shared_ptr<GroupNode> GroupNode::copyGroup() const
    {
      return copyGroup(entries());
    }

  }
}
