#include <idioma/ast/SourceLocation.h>

namespace idioma {
  namespace ast {

    SourceLocation::SourceLocation(::std::shared_ptr<const Source> xsource, Line xline,
        Column xcolumn)
        : source(xsource), line(xline), column(xcolumn)
    {
    }

    SourceLocation::SourceLocation(Line xline, Column xcolumn)
        : SourceLocation(nullptr, xline, xcolumn)
    {
    }

    SourceLocation::~SourceLocation()
    {
    }
  }
}
