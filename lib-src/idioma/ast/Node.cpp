#include <idioma/ast/Node.h>
#include <idioma/ast/GroupNode.h>
#include <idioma/ast/Visitor.h>
#include <idioma/ast/NodeVisitor.h>

#include <iostream>

namespace idioma {
  namespace ast {
    namespace {
      struct IgnorableKey
      {
        bool operator==(const IgnorableKey&) const
        {
          return true;
        }
      };
    }

    Node::Entry::Entry(const Relation &id, const CPtr &n)
        : rel(id), node(n)
    {
      if (!node) {
        throw ::std::invalid_argument("Missing node");
      }
    }
    Node::Entry::~Entry()
    {

    }

    Node::Node()
    {
    }

    Node::Node(Nodes cnodes)
        : nodes(::std::move(cnodes))
    {
    }

    Node::~Node()
    {
    }

    ::std::ostream& Node::print(::std::ostream &out) const
    {
      for (auto n : nodes) {
        n.node->print(out);
      }
      return out;
    }

    bool Node::getSpan(::std::shared_ptr<const SourceLocation> &start,
        ::std::shared_ptr<const SourceLocation> &end) const
    {
      for (auto n : nodes) {
        ::std::shared_ptr<const SourceLocation> xstart, xend;
        n.node->getSpan(xstart, xend);
        start = SourceLocation::minOf(start, xstart);
        end = SourceLocation::maxOf(end, xend);
      }
      return start && end;
    }

    Node::Span Node::span() const
    {
      ::std::shared_ptr<const SourceLocation> s, e;
      getSpan(s, e);
      return Span(s, e);
    }

    void Node::accept(NodeVisitor &v) const
    {
      Visitor *visitor = dynamic_cast<Visitor*>(&v);
      if (visitor) {
        visitor->visit(shared_from_this());
      }
    }

    void Node::visitChildren(NodeVisitor &v) const
    {
      for (auto n : nodes) {
        n.node->accept(v);
      }
    }

    void Node::annotate(const ::idioma::util::Key &key, const ::idioma::util::Value &value) const
    {
      for (KVPair &p : _annotations) {
        if (p.first == key) {
          p.second = value;
        }
      }
      _annotations.push_back(KVPair(key, value));
    }

    bool Node::hasAnnotation(const ::idioma::util::Key &key) const
    {
      for (const KVPair &p : _annotations) {
        if (p.first == key) {
          return true;
        }
      }
      return false;
    }

    ::idioma::util::Value Node::annotation(const ::idioma::util::Key &key) const
    {
      for (const KVPair &p : _annotations) {
        if (p.first == key) {
          return p.second;
        }
      }
      return ::idioma::util::Value();
    }

    void Node::annotate(const Node::Annotations &a) const
    {
      for (const KVPair &p : a) {
        annotate(p.first, p.second);
      }
    }

    Node::Annotations Node::annotations() const
    {
      return _annotations;
    }

    ::std::vector<Node::CPtr> Node::children() const
    {
      ::std::vector<CPtr> res;
      for (auto n : nodes) {
        res.push_back(n.node);
      }
      return res;
    }

    ::std::vector<Node::CPtr> Node::children(
        ::std::function<bool(const CPtr &node)> predicate) const
    {
      ::std::vector<CPtr> res;
      for (auto n : nodes) {
        if (predicate(n.node)) {
          res.push_back(n.node);
        }
      }
      return res;
    }

    Node::Relation Node::getIgnorableKey()
    {
      IgnorableKey k;
      return ::idioma::util::Key::create(k);
    }

    bool Node::isIgnorable() const
    {
      return false;
    }

    Node::CPtr Node::getNode(const Node::Relation &rel) const
    {
      for (auto e : nodes) {
        if (e.rel == rel) {
          return e.node;
        }
      }
      return nullptr;

    }

    ::std::vector<Node::CPtr> Node::getNodes(const Node::Relation &rel) const
    {
      ::std::vector<Node::CPtr> res;
      for (auto e : nodes) {
        if (e.rel == rel) {
          res.push_back(e.node);
        }
      }
      return res;
    }

  }
}

