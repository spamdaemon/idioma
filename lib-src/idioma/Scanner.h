#ifndef _IDIOMA_SCANNER_H
#define _IDIOMA_SCANNER_H

#ifndef _IDIOMA_SOURCE_H
#include <idioma/Source.h>
#endif

#include <iosfwd>
#include <memory>
#include <string>

namespace idioma {
  class Scanner
  {

    /** A location */
  public:
    struct Position
    {
      inline Position()
          : line(0), column(0)
      {
      }

      /**
       * Create a new line, and optionally reset the column
       */
      inline void newline(bool resetColumn)
      {
        ++line;
        if (resetColumn) {
          column = 0;
        }
      }

      inline void newcol()
      {
        ++column;
      }
      Source::CPtr source;
      size_t line, column;
    };

    /** The token produced by the scanner */
  public:
    struct Token
    {
      /** The default constructor creates a token with code 0 */
    public:
      Token();

      /** 
       * Create a token with the specified text and code.
       * @param xtext the token text
       * @param xcode the tokens numeric code
       */
    public:
      Token(const ::std::string &xtext, int xcode, const Position &xstart, const Position &xend);

      /**
       * Print this token to the given output stream
       * @param out an output stream
       */
    public:
      void print(::std::ostream &out) const;

      /**
       * Output operator
       */
      inline friend std::ostream& operator<<(std::ostream &out, const Token &t)
      {
        t.print(out);
        return out;
      }

      /** the token's integer code */
      const int code;

      /** The token's text */
      const std::string text;

      /** The start position */
      const Position start;

      /** End position */
      const Position end;
    };

    /**
     * Default constructor
     */
  protected:
    Scanner();

    /** Destructor */
  public:
    virtual ~Scanner();

    /**
     * Get the next token.
     * @return the next token or a token with code 0
     */
  public:
    virtual Token nextToken() = 0;

    /** 
     * Check if there were any errors while scanning.
     * @return true if there were errors
     */
  public:
    virtual bool hasErrors() const = 0;
  };
}

#endif
