#ifndef _IDIOMA_SCOPE_TREE_H
#define _IDIOMA_SCOPE_TREE_H

#ifndef _IDIOMA_SCOPE_SCOPE_H
#include <idioma/scope/Scope.h>
#endif

namespace idioma {
  namespace scope {

    /**
     * The tree manages a scope hierarchy. As long as the tree exists,
     * the scopes and any child scopes will remain.
     *
     * TODO:
     *  this needs to disappear
     */
    class Tree
    {
      /** The tree impl */
    private:
      struct Impl
      {
        Impl(Scope::Ptr root)
            : scope(root)
        {
        }
        ~Impl();

        Scope::Ptr scope;
      };

      /** The default constructor */
    public:
      Tree();

      /**
       * Get the root scope
       * @return the root scope
       */
    public:
      inline Scope::Ptr rootScope()
      {
        return _tree->scope;
      }

      /** The pointer to the scope handle */
    private:
      ::std::shared_ptr<Impl> _tree;

    };

  }
}
#endif
