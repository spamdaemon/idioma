#ifndef _IDIOMA_SCOPE_SCOPE_H
#define _IDIOMA_SCOPE_SCOPE_H

#ifndef _IDIOMA_UTIL_Key_H
#include <idioma/util/Key.h>
#endif

#ifndef _IDIOMA_UTIL_Value_H
#include <idioma/util/Value.h>
#endif

#include <memory>
#include <vector>
#include <utility>
#include <initializer_list>

namespace idioma {
  namespace scope {
    class Tree;

    /**
     * A simple scope class that works in combination with an AST
     * support upward traversal. Scopes only traverse upward.
     *
     * A scope is a recursive data structure that is associated with a
     * Tree. As long as the tree exists, the scope and all its child
     * scopes will also exist.
     *
     * TODO:
     *  this needs to be reworked and simplified
     */
    class Scope: public ::std::enable_shared_from_this<Scope>
    {
      /** A key */
    public:
      typedef ::idioma::util::Key Key;

      /** A key */
    public:
      typedef ::idioma::util::Value Value;

      /** The scope tree is a friend */
    public:
      friend class idioma::scope::Tree;
      /** The scope tree is a friend */

    private:
      typedef ::std::pair<Key, Value> KVPair;

      /**
       * The result of find a value. If not found, then the second field is false  */
    private:
      typedef ::std::pair<Value, bool> ValueResult;

      /** A scope pointer */
    public:
      typedef ::std::shared_ptr<Scope> Ptr;

      /** A scope pointer */
    public:
      typedef ::std::shared_ptr<const Scope> CPtr;

      /** Scope constructor */
    private:
      Scope(Ptr parent);

      /** Destructor */
    public:
      virtual ~Scope();

      /**
       * Create a root scope
       * @return a root scope
       */
    private:
      static Ptr createRoot();

      /**
       * Create a new child scope.
       * @return a scope
       */
    public:
      Ptr create();

      /**
       * Create a new child scope with the specified context.
       * @param context a set of context values
       * @return a scope
       */
    public:
      Ptr create(Value context);

      /**
       * Create a new child scope with the specified context.
       * @param contexts a set of context values
       * @return a scope
       */
    public:
      Ptr create(::std::initializer_list<Value> contexts);

      /**
       * Create a new child scope with a single entry
       * @param k a key
       * @param v the entry value.
       * @return a scope
       */
    public:
      Ptr create(const Key &k, const Value &v);

      /**
       * Get the level of this scope. The level is always the parent's level +1. If this
       * scope has no parent, then the level is defined to be 0.
       * @return the level associated with this scope.
       */
    public:
      inline size_t level() const
      {
        return _level;
      }

      /** The parent scope */
    public:
      inline ::std::shared_ptr<Scope> parent()
      {
        return _parent.lock();
      }

    private:
      const KVPair* findKey(const Key &k) const;
      KVPair* findKey(const Key &k);

      /**
       * Determine if this scope contains an entry for the specified key.
       * @param k a key
       * @return true if there is a entry for the specified key, false otherwise
       */
    public:
      inline bool containsKey(const Key &k) const
      {
        return findKey(k) != nullptr;
      }

      /**
       * Add a new key without a value.
       * @param k a key
       * @return true if the value was added, false if there was a previous value
       */
    public:
      virtual bool add(const Key &k);

      /**
       * Put a new entry into this scope or replace an existing entry..
       * @param k a key
       * @param v a value
       * @return true if the value was added, false if there was a previous value
       */
    public:
      virtual bool put(const Key &k, const Value &v);

      /**
       * Replace an existing entry for a key. If the key was found, then the entry is replaced and true is returned.
       * If the entry was added, then false is returned .
       * @param k a key
       * @param v a value
       * @return true if the value was updated, false if it was added
       */
    public:
      virtual bool update(const Key &k, const Value &v);

      /**
       * Find the entry with the specified key within this scope object. The hierarchy is not searched.
       * @param key a key
       * @param found true if the value was found.
       * @return a pointer to value object that holds an object of the specified type.
       */
    public:
      ValueResult find(const Key &k) const;

      /**
       * Lookup an entry in the hierarchy. This method is
       * equivalent to search(k)->get(k), if the key is found.
       * @param k a key
       * @return a value (maybe unset if not found)
       */
    public:
      ValueResult lookup(const Key &k) const;

      /**
       * Search for a scope in which the specified key is defined.
       * @param k a key
       * @return a scope or nullptr
       */
    public:
      inline CPtr search(const Key &k) const;

      /**
       * Search for a scope in which the specified key is defined.
       * @param k a key
       * @return a scope or nullptr
       */
    public:
      Ptr search(const Key &k);

      /**
       * Find a scope with the specified context.
       * @return a scope object.
       */
    public:
      template<class T>
      ::std::pair<::std::shared_ptr<Value::Object<T> >, bool> getContext() const
      {
        CPtr scope = shared_from_this();
        for (Value v : scope->_context) {
          auto ctx = v.cast<T>();
          if (ctx) {
            return ::std::pair<::std::shared_ptr<Value::Object<T> >, bool>(ctx, true);
          }
        }
        return ::std::pair<::std::shared_ptr<Value::Object<T> >, bool>(nullptr, false);
      }

      /**
       * Find a scope with the specified context.
       * @return a scope object.
       */
    public:
      template<class T>
      ::std::pair<::std::shared_ptr<Value::Object<T> >, bool> findContext() const
      {
        CPtr scope = shared_from_this();
        while (scope) {
          auto res = scope->getContext<T>();
          if (res.second) {
            return res;
          }
          scope = scope->_parent.lock();
        }
        return ::std::pair<::std::shared_ptr<Value::Object<T> >, bool>(nullptr, false);
      }

      /**
       * Find a scope with the specified context.
       * @return a scope object.
       */
    public:
      template<class T> Ptr findContextScope()
      {
        Ptr scope = shared_from_this();
        while (scope) {
          for (Value v : scope->_context) {
            auto ctx = v.cast<T>();
            if (ctx) {
              return scope;
            }
          }
          scope = scope->_parent.lock();
        }
        return nullptr;
      }
    public:
      template<class T> CPtr findContextScope() const
      {
        CPtr scope = shared_from_this();
        while (scope) {
          for (Value v : scope->_context) {
            auto ctx = v.cast<T>();
            if (ctx) {
              return scope;
            }
          }
          scope = scope->_parent.lock();
        }
        return nullptr;
      }

      /** Destroy this scope and any children recursively */
    private:
      void destroy();

      /** The parent scope */
    private:
      const ::std::weak_ptr<Scope> _parent;

      /** The level of this scope */
    private:
      const size_t _level;

      /** Contexts */
    private:
      ::std::vector<Value> _context;

      /** The child scopes */
    private:
      ::std::vector<Ptr> _children;

      /** The value */
    private:
      ::std::vector<KVPair> _entries;
    };
  }
}
#endif
