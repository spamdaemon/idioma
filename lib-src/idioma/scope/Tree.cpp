#include <idioma/scope/Tree.h>

namespace idioma {
  namespace scope {
    Tree::Impl::~Impl()
    {
      scope->destroy();
    }

    Tree::Tree()
        : _tree(new Impl(Scope::createRoot()))
    {
    }

  }
}
