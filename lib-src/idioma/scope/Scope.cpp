#include <idioma/scope/Scope.h>

namespace idioma {
  namespace scope {
    Scope::Scope(Ptr ptr)
        : _parent(ptr), _level(ptr ? (ptr->_level + 1) : 0)
    {
    }

    Scope::~Scope()
    {
    }

    void Scope::destroy()
    {
      _context.clear();
      _entries.clear();
      while (!_children.empty()) {
        Ptr c = _children.back();
        _children.pop_back();
        c->destroy();
      }
      _children.clear();
    }

    Scope::Ptr Scope::createRoot()
    {
      return Ptr(new Scope(nullptr));
    }

    Scope::Ptr Scope::create(::std::initializer_list<Value> context)
    {
      Ptr c(new Scope(shared_from_this()));
      c->_context = context;
      _children.push_back(c);
      return c;
    }

    Scope::Ptr Scope::create(Value context)
    {
      return create( { context });
    }

    Scope::Ptr Scope::create()
    {
      Ptr c(new Scope(shared_from_this()));
      _children.push_back(c);
      return c;
    }

    Scope::Ptr Scope::create(const Key &k, const Value &v)
    {
      Ptr s = create();
      s->_entries.push_back(KVPair(k, v));
      return s;
    }

    const Scope::KVPair* Scope::findKey(const Key &k) const
    {
      for (auto &kv : _entries) {
        if (kv.first == k) {
          return &kv;
        }
      }
      return nullptr;
    }

    Scope::KVPair* Scope::findKey(const Key &k)
    {
      for (auto &kv : _entries) {
        if (kv.first == k) {
          return &kv;
        }
      }
      return nullptr;
    }

    Scope::ValueResult Scope::find(const Key &k) const
    {
      ValueResult res;
      auto kv = findKey(k);
      if (kv) {
        res.first = kv->second;
        res.second = true;
      }
      return res;
    }
    bool Scope::add(const Key &k)
    {
      return put(k, Value());
    }

    bool Scope::put(const Key &k, const Value &v)
    {
      if (containsKey(k)) {
        return false;
      }
      _entries.push_back(KVPair(k, v));
      return true;
    }

    bool Scope::update(const Key &k, const Value &v)
    {
      auto kv = findKey(k);
      if (kv) {
        kv->second = v;
        return true;
      }
      _entries.push_back(KVPair(k, v));
      return false;
    }

    Scope::CPtr Scope::search(const Key &k) const
    {
      auto scope = shared_from_this();
      while (scope && !scope->containsKey(k)) {
        scope = scope->_parent.lock();
      }
      return scope;
    }

    Scope::Ptr Scope::search(const Key &k)
    {
      auto scope = shared_from_this();
      while (scope && !scope->containsKey(k)) {
        scope = scope->_parent.lock();
      }
      return scope;
    }

    Scope::ValueResult Scope::lookup(const Key &k) const
    {
      ValueResult res(nullptr, false);

      auto scope = shared_from_this();
      while (scope) {
        auto kv = scope->findKey(k);
        if (kv) {
          res.first = kv->second;
          res.second = true;
          break;
        }
        scope = scope->_parent.lock();
      }
      return res;
    }

  }
}
