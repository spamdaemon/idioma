#include "Scanner.h"
#include <iostream>

namespace idioma {
  Scanner::Scanner()
  {
  }
  Scanner::~Scanner()
  {
  }

  Scanner::Token::Token()
      : code(0), start(), end()
  {
  }

  Scanner::Token::Token(const ::std::string &xtext, int xcode, const Scanner::Position &xstart,
      const Scanner::Position &xend)
      : code(xcode), text(xtext), start(xstart), end(xend)
  {
  }

  void Scanner::Token::print(std::ostream &out) const
  {
    out << text;
  }

}
