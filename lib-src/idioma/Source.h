#ifndef _IDIOMA_SOURCE_H
#define _IDIOMA_SOURCE_H

#include <memory>
#include <ostream>
#include <string>

namespace idioma {

  /**
   * An abstract class that represents an input source.
   */
  class Source
  {
    /** A pointer to a source */
  public:
    typedef ::std::shared_ptr<const Source> CPtr;

    virtual ~Source();

    /** Print the source */
  public:
    virtual void print(::std::ostream &out) const;

    /**
     * Create a source with from some kind of identitier.
     * @param src
     * @return a source object
     */
  public:
    static CPtr create(const ::std::string &src);
  };
}
#endif
