#include <idioma/Parser.h>
#include <string>
#include <iostream>

namespace idioma {
  namespace {
    static ::std::string readFully(::std::istream &in)
    {
      ::std::istreambuf_iterator<::std::string::value_type> begin(in), end;
      ::std::string res(begin, end);
      return res;
    }

  }
  Parser::Parser()
  {
  }

  Parser::~Parser()
  {
  }

  idioma::parser::ParseTree::Ptr Parser::parseStream(::std::istream &stream,
      const ::std::string &sourceInfo)
  {
    return parseText(readFully(stream), sourceInfo);
  }

}

