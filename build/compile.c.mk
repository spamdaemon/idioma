# this fragment defines how to compile source code 
# it defines functions such as compile.cpp, compile.c 
# to compile C++ and C code

define compile.c
	gcc -c -fPIC -o $@ $< \
	$(C_WARNING_OPTS)\
	$(C_STANDARD_OPTS) \
	$(EXT_C_OPTS) \
        $(if $(filter true,$(COVERAGE)), $(COMPILER_COVERAGE_OPTS)) \
	$(if $(filter true,$(OPTIMIZE)), $(C_OPTIMIZE_OPTS),$(C_DEBUG_OPTS)) \
	$(addprefix -I,$(BASE_INC_SEARCH_PATH) $(PROJ_INC_SEARCH_PATH) $(EXT_INC_SEARCH_PATH)) \
	$(addprefix -D,$(C_DEFINES) $(PROJ_COMPILER_DEFINES) $(EXT_COMPILER_DEFINES))
endef


define compile.test.c
	gcc -c -fPIC -o $@ $< \
	$(C_WARNING_OPTS)\
	$(C_STANDARD_OPTS) \
	$(EXT_C_OPTS) \
	$(C_DEBUG_OPTS) \
	$(addprefix -I,$(BASE_INC_SEARCH_PATH) $(PROJ_INC_SEARCH_PATH) $(EXT_INC_SEARCH_PATH)) \
	$(addprefix -D,$(C_DEFINES) $(PROJ_COMPILER_DEFINES) $(EXT_COMPILER_DEFINES))
endef

define make.depend.c
	gcc -MM -MG -MT $(patsubst %.depend,%.o,$@) -MF $@ $< \
	$(C_WARNING_OPTS)\
	$(C_STANDARD_OPTS) \
	$(EXT_C_OPTS) \
 	$(addprefix -I,$(BASE_INC_SEARCH_PATH) $(PROJ_INC_SEARCH_PATH) $(EXT_INC_SEARCH_PATH)) \
	$(addprefix -D,$(C_DEFINES) $(PROJ_COMPILER_DEFINES) $(EXT_COMPILER_DEFINES))
endef
